<!-- BEST VIEWED KATEX RENDERED -->

# 4B25 - Tackling Unknowns of [Raise to Wake](https://support.apple.com/en-us/108325)  

[[*] This README is best viewed using a supported browser/web renderer via Git{Hub,Lab} - available here :D](https://gitlab.com/js2597/4b25b/-/blob/main/README.md)  

| Name           | CRSid      | College  |
| -------------- | ---------- | -------- |
| Stephen J. Sun | **js2597** | HO       |

<!-- <img src="doc/src/intro.svg" width="100" height="100"> -->
<!-- ![Text](data:image/svg+xml,[data]) -->
![intro](doc/src/intro.svg)
[*] Axes as per the conventions of mounting the accelerometer on the **FRDM-KL03Z** board or mobile devices.

**Abstract:**

This project provides an orientation detection mechanism with quantifiable confidence using the built-in accelerometer **MMA8451Q** on the **[FRDM-KL03Z](https://www.nxp.com/design/design-center/development-boards/general-purpose-mcus/freedom-development-platform-for-kinetis-kl03-mcus:FRDM-KL03Z)** development board. 

## Motivation

Energy efficiency is key in embedded systems. **Raise to Wake** allows a device to stay in a low-power state until the accelerometer triggers an interrupt or when the host processor responds to a recognised motion pattern. This project explores a resource-efficient design with an algorithm that allows for checking the fidelity of this mechanism.

## Key Design

### 1. Determining Orientation

Orientation detection is designed to perform a two-fold notify-verify process, where the accelerometer instructs the main processor to wake up while simultaneously retaining the ability to provide a live readout of the orientation based on the accelerometer data. An estimation of confidence is provided, incorporating the quantification of the system's current linear acceleration with gravity filtered out, as well as uncertainty propagation from intrinsic accelerometer noise floor.

The measurements provided by a MEMS-based accelerometer may be mathematically expressed as,

```math
a = \begin{bmatrix} X \\ Y \\ Z \end{bmatrix} = R(g-a_{r}) \approx R\begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix} = R_{x}(\theta)R_{y}(\phi)R_{z}(\psi)\begin{bmatrix} 0 \\ 0 \\ 1 \end{bmatrix}
```

where $R$ is the rotation matrix, $a_{r}$ is the acceleration due to gravity, and $R_{x}(\theta)$, $R_{y}(\phi)$, and $R_{z}(\psi)$ are the rotation matrices for pitch, roll, and yaw, about the x, y, and z axes respectively. Specifically,
```math
R_{x} = \begin{bmatrix} 1 & 0 & 0 \\  0 & \cos\theta & -\sin\theta \\  0 & \sin\theta & \cos\theta \end{bmatrix}
```
```math
R_{y} = \begin{bmatrix} \cos\phi & 0 & \sin\phi \\  0 & 1 & 0 \\  -\sin\phi & 0 & \cos\phi \end{bmatrix}
```
```math
R_{z} = \begin{bmatrix} \cos\psi & -\sin\psi & 0 \\  \sin\psi & \cos\psi & 0 \\  0 & 0 & 1 \end{bmatrix}
```
```math
 \implies \frac{a}{\|a\|} = \frac{1}{\sqrt{X^{2}+Y^{2}+Z^{2}}}\begin{bmatrix} X \\  Y \\  Z \end{bmatrix} = \begin{bmatrix} \sin\phi \\  -\sin\theta\cos\phi \\  \cos\theta\cos\phi \end{bmatrix}^\ast
 ```
Therefore, the pitch and roll angles $\theta$ and $\phi$ can be expressed as,
$$ \theta = \arctan\left(\frac{-Y}{Z} \right) \quad; \quad \phi = \arctan\left(\frac{X}{\sqrt{Y^{2}+Z^{2}}} \right) $$  
Optionally, it is also possible to estimate the total tilt from the $z$-axis,
$$ \rho = \arccos\left(\frac{Z}{\|a\|} \right) = \arccos\left(\frac{Z}{\sqrt{X^{2}+Y^{2}+Z^{2}}}\right) $$  

[*] The yaw angle $\psi$ would require an additional magnetometer to be determined.

### 2. Propagation of Uncertainty

Raw accelerometer data can have a certain level of noise. If the standard deviation of the Gaussian noise is $\sigma$, then the translated uncertainty in pitch and roll angles can be approximated by taking Taylor Series expansions of the original equations.
$$ \sigma_{\theta} \approx \frac{\sigma_{Y/Z}}{1+\left(\frac{Y}{Z}\right)^2} \approx \frac{|\frac{Y}{Z}|\sqrt{\frac{\sigma_{Y}^2}{Y^2} + \frac{\sigma_{Z}^2}{Z^2} - 2\frac{\sigma_{YZ}}{YZ}}}{1 + \left(\frac{Y}{Z}\right)^2} $$
$$ \sigma_{\phi} \approx \frac{\sigma_{X/\sqrt{Y^2+Z^2}}}{1+\left(\frac{X}{\sqrt{Y^2+Z^2}}\right)^2} \approx \frac{|\frac{X}{\sqrt{Y^2+Z^2}}|\sqrt{\frac{\sigma_{X}^2}{X^2} + \frac{Y^2\sigma_{Y}^2 + Z^2\sigma_{Z}^2}{(Y^2+Z^2)^2}}}{1 + \left(\frac{X}{\sqrt{Y^2+Z^2}}\right)^2} $$

### 3. Quantifying Probabilities

The noise model used for acceleration data is Gaussian with some mean and standard deviation. It therefore follows that the distribution in pitch and roll should also be Gaussian, and if noise is additive on the measurand, the measurement model can be effectively characterised by,

$$ Pr(X=x) \approx \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-x(t))^2}{2\sigma^2}} \approx \frac{1}{\sqrt{2\pi\sigma^2}}e^{-\frac{(x-(x_1-x_0)\cdot H(t-t_0))^2}{2\sigma^2}} $$

If the target orientation is an interval rather than a single value, the probability can be estimated by integrating the probability mass function (PMF) of pitch angle (as well as roll angle). 

### 4. Confidence Estimation

The **Raise to Wake** event to be detected can be interpreted as a step change from a flat state, i.e. [-14°, 14°] to a vertical state, i.e. [45°, 135°]. Since confidence level is quantified by when the system returns a verdict for the event that has happened, i.e. whether the device has been rotated or not, it is imperative to know how certain it is of the conclusion when given the prior.

- a. Confidence (of correct identification) when the system reports back 'detected' (**True Positives**)

The probability of the system identifying **Raise to Wake** when it has indeed occurred can be approximated by the probability of the system successively idenifying the flat and rotated states correctly.

$$ Pr(\text{Detected}|\text{Ocurred}) = Pr_1 \cdot Pr_2 $$  
```math
Pr_1 = Pr(|\theta_{d,1}|≤14° \mid |\theta_{1}|≤14°) \approx \int_{-14}^{14} Pr(\theta_{1})d\theta_{1}
```  
```math
Pr_2 = Pr(45°≤|\theta_{d,2}|<135° \mid 45°≤|\theta_{2}|<135°)
```

- b. Confidence (of correct identification) when the system reports back 'undetected' (**True Negatives**)

$$ Pr(\text{Not Detected}|\text{Not Occurred}) = Pr_1' \cdot Pr_2' $$
```math
Pr_1' = Pr(|\theta_{d,1}|>14° \mid |\theta_{1}|>14°) \approx \int_{14}^{180} Pr(\theta_{1})d\theta_{1} + \int_{-180}^{-14} Pr(\theta_{1})d\theta_{1}
```
```math
Pr_2' = Pr(|\theta_{d,2}|<45° \mid |\theta_{2}|<45°) + Pr(|\theta_{d,2}|≥135° \mid |\theta_{2}|≥135°)
```

And as expected, given all possible outcomes the total probability should sum to 1, i.e.

$$ Pr(\text{TP|Occurred}) + Pr(\text{TN|Not Occurred}) + Pr(\text{FP|Not Occurred}) + Pr(\text{FN|Occurred}) = 1 $$  

where **FP** and **FN** are the probabilities of false positives and false negatives respectively.


## Implementation

Due to memory constraints of embedded C, core functions are implemented manually where possible instead of relying on the C standard `math.h` library. Some examples are provided below,

- ### Power
As only integer exponentiation is required, this has been optimised for $x^n$ where $x\in\mathbb{R}$ and $n\in\mathbb{N}$.
```C
for(int i = 0; i < exp; i++) { res *= base; }
```
- ### Square Root
The degree of accuracy has been limited further for faster embedded computation.
```C
while ((test * test - num) >= lim) { test = (test + num / test) / 2; }
```
- ### Trigonometric Functions
The `arctan` function uses a rather truncated Taylor Series to calculate the angle.
```C
double sum = 0.0;
for(int i = 0; i < 2; i++) {
    double term = ((i % 2 == 0 ? 1 : -1) * power(ratio, 2 * i + 1)) / (2 * i + 1);
    sum += term;
}
```
A hard-coded LUT approach `arctan_hc` was also included for even faster computation if required. However, to satisfy the higher rate it would introduce a penalty in terms of precision.

## Evaluation

### 1. Sensor Noise & Performance

The resolution of the sensor is often of interest. However, without knowing how much noise there is, it is difficult to ascertain the smallest detectable change in acceleration the sensor can detect. 

To characterise the amount of Gaussian noise and optimise it for this application, 5,000 samples have been taken under each mode, to evaluate the trade-offs between higher resolution, higher sample rate and ideally lower power consumption, due to the nature of this application.

| ODR $(Hz)$ | RMS Noise $(mg)$ | Resolution $(mg)$ | Current $(\mu A)$ | ENOB  |
|------------|------------------|-------------------|-------------------|-------|
| 50 (LN)    | 0.70             | 2.82              | 20                | 10.68 |
| 800 (LN)   | 2.11             | 8.45              | 165               | 9.10  |
| 800 (NORM) | 2.82             | 11.28             | 200               | 8.68  |

- Distribution and spread of 5,760 $z$-axis measurements sampled at **50 Hz**, with board fixed:  
![50](doc/src/normplot_50.svg)

- Distribution and spread of 5,760 $z$-axis measurements sampled at **800 Hz**, with board fixed:  
![800](doc/src/normplot_800.svg)

In both cases, a perfect Gaussian distribution was fitted (in black). The 50 Hz mode conforms more closely to the Gaussian distribution with lower noise levels. This was also observed in the skewness, kurtosis, and Q-Q plot for the $z$-axis samples. 

![skewness](doc/src/skewness.svg)
![kurtosis](doc/src/kurtosis.svg)
![qqplot](doc/src/qqplot.svg)

### 2. Monte Carlo Simulation of Probabilities

By assuming the noise model for the acceleration data to be Gaussian, Monte Carlo simulations are performed to confirm the spread of measured pitch for a fixed acceleration input under noise, and to check the validity of applying the uncertainty propagation formulae. By applying known means of $\mu_{Y}$ and $\mu_{Z}$ being 1200 and 2078, the estimated mean for pitch should be -30°. The result of the simulation is compared with the Gaussian distribution recreated from the uncertainty propagation formulae (shown below in black), indicating less than 1% error.

![theta_sim](doc/src/theta_sim.svg)

To confirm the effect of integrating over a range of values, further simulations were performed to check the probability of measured pitch being within the range of [-14°, 14°] and [45°, 135°], with the injected noise causing a standard deviation of 0.00122g (corresponding to the previously measured standard deviation of $4.8/4096$ when the sensor is stationary) at a total acceleration of exactly 1g. A simulation was needed since the standard deviation in pitch is also a function of the mean angle. The input angles were set to be uniformly distributed within both ranges.

![sim_1](doc/src/sim_1.svg)
![sim_2](doc/src/sim_2.svg)

These results yield a true postive rate of $99.87\% \cdot 99.81\% = 99.7\%$ if the accelerometer is stationary in both cases with no additional linear acceleration. However, this is usually not the case and it was subsequently found that increasing total linear accleration can significantly degrade the accuracy.

### 3. Improved Implementation

A more embedded-friendly implementation offloads the actual calculations to the **MMA8451Q** sensor itself and leverages the built-in debounce threshold, high pass filter, and above 1.25g lockout to remove effects of linear acceleration noise. Since the host processor is only interested in accurately receiving wake-up events, the interrupts were also designed to include a hysteresis range of 28° to prevent false positives. The final implementation leverages this, allowing the system to be much more immune to small amounts of additional noise while retaining much of the energy efficiency benefits. The updated threshold, mode, and interrupt settings for this application are defined in [`src/boot/ksdk1.1.0/boot.c`](src/boot/ksdk1.1.0/boot.c) and prescribed to the sensor during its initial configuration. Additional code leverages this to also provide information on the portrait or landscape positioning of the device, and prints out **PU**, **PD**, **LL**, **LR** for **Portrait Up**, **Portrait Down**, **Landscape Left**, and **Landscape Right** modes respectively.

## Getting Started

### 0. Set up the repository
- Ensure you have working installations of `arm-none-eabi-gcc` toolchain (required for building), `JLinkExe` and `JLinkRTTClient` from SEGGER. The default target assumes you have `make`, `cmake`, `sed`, `screen` in your $PATH. 
- [Optional] Clone this repository. Skip this if source files are provided already. 
```bash
git clone git@gitlab.com:js2597/4b25b.git; cd 4b25b
```
- Customise the following directory paths depending on your setup.
```bash
export ARMGCC_DIR=[DIR/arm-none-eabi] JLINK_DIR=[DIR/JLink_VER]
export JLINKPATH=$JLINK_DIR/JLinkExe
```
```bash
sed -i '' -e "s|JLINKPATH.*|JLINKPATH=$JLINKPATH|g" setup.conf
sed -i '' -e "s|ARMGCC_DIR.*|ARMGCC_DIR=$ARMGCC_DIR|g" setup.conf
set -a; source setup.conf
```

### 1. [Optional] Build the firmware
```bash
make clean; make frdmkl03
```
### 2. Load the firmware
If compiling from source,
```bash
screen -dmS load make load-warp
```
Or if using the pre-built firmware,
```bash
screen -dmS load make load-generic
```
### 3. Interact with the device   
```bash
$JLINK_DIR/JLinkRTTClient
```

## Repository Structure
[*] For coursework submission purposes, this repository comprises the bare minimum source code required to compile all targets and has been stripped down to keep the core components for building the firmware only.  

- [`build`](build/) stores temporary build artifacts
- [`doc/src/`](doc/src/) contains sources for the documentation
- [`doc/src/*.csv`](doc/src/flat_10010_00.csv) contains recorded data for calibration
- [`doc/src/run.ipynb`](doc/src/run.ipynb) core Jupyter notebook used for evaluation
- [`src/boot/ksdk1.1.0`](src/boot/ksdk1.1.0/) contains core source code of the project
- [`src/boot/ksdk1.1.0/boot.c`](src/boot/ksdk1.1.0/boot.c) contains the firwmare entry point
- [`src/boot/ksdk1.1.0/devMMA8451Q.c`](src/boot/ksdk1.1.0/devMMA8451Q.c) contains driver for the accelerometer

## References

**[1]** [MMA8451Q Datasheet](https://www.nxp.com/docs/en/data-sheet/MMA8451Q.pdf)  
**[2]** [AN3447 Implementing a Tilt-Compensated eCompass using Accelerometer and Magnetometer Sensors](https://www.nxp.com/docs/en/application-note/AN3447.pdf)  
**[3]** [AN3461 Tilt Sensing Using a Three-Axis Accelerometer](https://www.nxp.com/docs/en/application-note/AN3461.pdf)  
**[4]** [AN4068 Embedded Orientation Detection](https://www.nxp.com/docs/en/application-note/AN4068.pdf)  
**[5]** [AN4073 FIFO Buffer Mode](https://www.nxp.com/docs/en/application-note/AN4073.pdf)  
**[6]** [AN4075 High Resolution and Low Power](https://www.nxp.com/docs/en/application-note/AN4075.pdf)  
**[7]** [FRDM-KL03Z User Guide](https://www.nxp.com/docs/en/user-guide/FRDMKL03ZUG.pdf)  
**[8]** [An Algorithm for Sensor Data Uncertainty Quantiﬁcation](https://ieeexplore.ieee.org/document/9647933)  
**[9]** [Error-Efﬁcient Computing Systems](https://people.csail.mit.edu/rinard/paper/fteda17.pdf)  
**[10]** [Reliability Model for MEMS Accelerometers](https://core.ac.uk/download/pdf/52955684.pdf)  